from flask import Flask, request, render_template
from solc import compile_source,compile_files
from web3 import Web3, HTTPProvider
from web3.contract import ConciseContract#so that we can call the function name directly
import binascii
import redis
import json

app = Flask(__name__)
r = redis.StrictRedis()

http_provider = HTTPProvider('http://localhost:7545')
eth_provider = Web3(http_provider).eth

#default_account = eth_provider.accounts[0]
eth_provider.defaultAccount = eth_provider.accounts[0]

#transaction_details = {'from':default_account}

with open('calc.sol') as file:
	scode = file.readlines()

#compile the contract
compiled_code = compile_source(''.join(scode))
rrcoincompiled_code = compile_files(["RRCoin.sol"])

contract_name = 'calc'

calccontract = compiled_code['<stdin>:calc']
rrcoincontract = rrcoincompiled_code['RRCoin.sol:RRToken']

bytecode = calccontract['bin']
abi = calccontract['abi']

rrcoinbytecode = rrcoincontract['bin']
rrcoinabi = rrcoincontract['abi']

##compile with compile_files example anf get the bin and abi
##1)from solc import compile_files
##2)compiled_code = compile_files(["RRCoin.sol"])
##3)compiled_code.keys()#get the keys
##4)compiled_code['RRCoin.sol:RRToken']['bin'] #get the bin
##5)compiled_code['RRCoin.sol:RRToken']['abi'] #get the abi

#use this to deploy any number of instance of the contract on the chain
contractfactory = eth_provider.contract(abi=abi,bytecode=bytecode)
rrcoincontractfactory = eth_provider.contract(abi=rrcoinabi,bytecode=rrcoinbytecode)

tx_hash = contractfactory.constructor().transact()
rrcointx_hash = rrcoincontractfactory.constructor().transact()

#get tx receipt to get contract address
tx_receipt = eth_provider.getTransactionReceipt(tx_hash)
contractaddr = tx_receipt['contractAddress']

rrcointx_receipt = eth_provider.getTransactionReceipt(rrcointx_hash)
rrcoincontractaddr = rrcointx_receipt['contractAddress']

calccontractobj = eth_provider.contract(abi=abi,address=contractaddr,ContractFactoryClass=ConciseContract)

rrcoincontractobj = eth_provider.contract(abi=rrcoinabi,address=rrcoincontractaddr,ContractFactoryClass=ConciseContract)

accbal = Web3.fromWei(eth_provider.getBalance(eth_provider.defaultAccount),'ether')

@app.route('/',methods=['GET','POST'])
def index():
	thisaddr = ''
	tmstamp = calccontractobj.getDayAfter()
	theval = calccontractobj.getData(1)
	kval = request.form.get('kval')
	setval = request.form.get('setval')
	if request.method == 'POST' and kval:
		try:
			theval = calccontractobj.getData(int(kval))
			thisaddr = calccontractobj.getAddress()
			print(eth_provider.accounts[1])
		except ValueError:
			alert = f'Some error'
	elif request.method == 'POST' and setval:
		try:
			calccontractobj.setData(int(setval),transact={'from':eth_provider.defaultAccount})
			print(f'value set {setval}')
		except ValueError:
			alert = f'invalid value'

	return render_template('calcindex.html',accountbal=int(round(accbal)),caddr=thisaddr,theval=theval,bheight=eth_provider.blockNumber,btstamp=tmstamp)

@app.route("/survey/<userhash>",methods=['GET','POST'])
def survey(userhash=None):
	user1b = binascii.hexlify(b'user1')
	if request.method == 'POST':
		gender = request.form.get('gender')
		vehicle = request.form.getlist('vehicle')
		print(gender,vehicle)
		userdata = json.loads(r.get(userhash))
		userdata['vote'] = {'vehicle':vehicle,'gender':gender}
		userdatas = json.dumps(userdata)
		r.set(userhash,userdatas)
		rrcoinbalance = rrcoincontractobj.balanceOf(eth_provider.defaultAccount)
		result = rrcoincontractobj.surveyAnswered(user1b,transact={'from':eth_provider.defaultAccount})
		getrrcoin = rrcoincontractobj.getRewardBalance(user1b)
		print(result,getrrcoin,rrcoinbalance)
		return render_template('survey.html',rrcoin=getrrcoin)
	else:
		getrrcoin = rrcoincontractobj.getRewardBalance(user1b)
		if getrrcoin:
			return render_template('survey.html',rrcoin=getrrcoin)
		else:
			return render_template('survey.html',rrcoin=0)

if __name__ == '__main__':
	app.run(debug=True)