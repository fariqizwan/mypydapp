pragma solidity ^0.4.17;
 
 import 'openzeppelin/ERC20/StandardToken.sol';
 
 contract RRToken is StandardToken {

 	 mapping(bytes32 => uint256) reward;
     string public name = 'RRCoin';
     string public symbol = 'RR';
     uint8 public decimals = 2;
     uint public INITIAL_SUPPLY = 50000;
     uint public totalSupply;
     uint8 private rewardvalue = 1;
 
     function RRToken() public {
         totalSupply = INITIAL_SUPPLY;
         balances[msg.sender] = INITIAL_SUPPLY;
     }
     /*Add 1 coin to user account */
     function surveyAnswered(bytes32 userid) public returns (bool success){
     	balances[msg.sender] = balances[msg.sender].sub(rewardvalue);
     	reward[userid] = reward[userid].add(rewardvalue);
     	return true;
     }

     function getRewardBalance(bytes32 userid) public view returns (uint256 rewardbalance){
     	return reward[userid];
     }
 }
