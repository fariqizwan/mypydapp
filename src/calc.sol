pragma solidity ^0.4.19;

contract calc {
	mapping (uint8 => uint) public data;
	uint8 private index = 1;
	event Setevent(address indexed _from,uint8 _x);

function calc() public {
	data[index] = 1000;
}

function getData(uint8 num) public view returns (uint) {
	return data[num];
}

function getAddress() public view returns (address) {
	return this;
}

function getDayAfter() public view returns (uint) {
    return block.timestamp + 1 days;
}

function setData(uint8 newnum) public {
	index += 1;
	data[index] = newnum;
	Setevent(msg.sender,newnum);
}

}

//0xFC2452E33d1e8b52170b7409b413433544C628A9 (contract address in Ropsten Test Net) 