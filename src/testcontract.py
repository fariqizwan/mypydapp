from solc import compile_source
from web3 import Web3, HTTPProvider
from web3.contract import ConciseContract

http_provider = HTTPProvider('http://localhost:7545')
eth_provider = Web3(http_provider).eth

contract_source_code = '''
pragma solidity ^0.4.19;

contract Greeter {
    string public greeting;

    function Greeter() public {
        greeting = 'Hello';
    }

    function setGreeting(string _greeting) public {
        greeting = _greeting;
    }

    function greet() view public returns (string) {
        return greeting;
    }
}
'''

compiled_sol = compile_source(contract_source_code)
contract_interface = compiled_sol['<stdin>:Greeter']
eth_provider.defaultAccount = eth_provider.accounts[0]

# Instantiate and deploy contract
Greeter = eth_provider.contract(abi=contract_interface['abi'], bytecode=contract_interface['bin'])

# Submit the transaction that deploys the contract
tx_hash = Greeter.constructor().transact()

# Wait for the transaction to be mined, and get the transaction receipt
tx_receipt = eth_provider.waitForTransactionReceipt(tx_hash)

# Create the contract instance with the newly-deployed address
greeter = eth_provider.contract(
    address=tx_receipt.contractAddress,
    abi=contract_interface['abi'],
ContractFactoryClass=ConciseContract)

print('Default contract greeting: {}'.format(greeter.greet()))
